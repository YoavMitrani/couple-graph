import sys

import pandas as pd
import pyvis.network
from pyvis.network import Network
import networkx as nx
import webbrowser
import os
import time
import statistics
import math
import json

ADD_NODE = "1"
ADD_LINE = "2"
REMOVE_NODE = "3"
REMOVE_LINE = "4"
CHANGE_NODE = "5"
CHANGE_LINE = "6"
SHORTEST_PATH = "7"
CHANGE_SCORE = "8"
SHOW_GRAPH = "9"
SAVE = "10"
EXIT = "11"

DB_NAME = "database.json"
FILE_NAME = "graph.html"

def main() -> None:
    clear()
    global typeTable
    pyvisG = None
    nxG = None
    try:
        df = pd.read_json(DB_NAME)
    except:
        df = pd.DataFrame.from_dict({'Source': [], 'Target': [], 'Type': []})
    try:
        with open('typeTable.json', 'r') as jFile:
            typeTable = json.load(jFile)
    except:
        with open('typeTable.json', "w") as jFile:
            json.dump(typeTable, jFile)
    inp = 0
    while inp != EXIT:
        clear()
        print_menu()
        inp = input()
        clear()
        if inp == ADD_NODE:
            print("What name would you like to add?")
            df = add_node(df, input())
            print("\nDone")
        elif inp == ADD_LINE:
            print("From whom?")
            nameSrc = input()
            print("\nTo whom?")
            nameDst = input()
            print("\nWhat type of relationship?")
            print("From the options: " + ", ".join(list(typeTable.keys())[:-1]))
            rel = input()
            if rel in typeTable.keys():
                df = add_edge(df, nameSrc, nameDst, rel)
                print("\nDone")
            else:
                print("Not a valid relationship status")
        elif inp == REMOVE_NODE:
            print("Which node?")
            nameSrc = input()
            df = df[df['Source'] != nameSrc][df['Target'] != nameSrc]
            print("\nDone")
        elif inp == REMOVE_LINE:
            print("From whom?")
            nameSrc = input()
            print("\nTo whom?")
            nameDst = input()
            df = df[(df['Source'] != nameSrc) | (df['Target'] != nameDst)]
            print("\nDone")
        elif inp == CHANGE_NODE:
            print("What name would you like to change?")
            nameSrc = input()
            print("\nTo what?")
            nameDst = input()
            change_name(df, nameSrc, nameDst)
            print("\nDone.")
        elif inp == CHANGE_LINE:
            print("From whom?")
            nameSrc = input()
            print("\nTo Whom?")
            print("From the options: " + ", ".join(list(df.loc[df["Source"] == nameSrc, "Target"])))
            nameDst = input()
            print("\nWhat type of relationship?")
            print("From the options: " + ", ".join(list(typeTable.keys())[:-1]))
            rel = input()
            if rel in typeTable.keys():
                change_line(df, nameSrc, nameDst, rel)
                print("\nDone")
            else:
                print("Not a valid relationship status")
        elif inp == SHORTEST_PATH:
            print("From whom?")
            nameSrc = input()
            print("\nTo whom?")
            nameDst = input()
            if nxG is None:
                nxG = create_nx_graph(df)
            print(shortest_path(nxG, nameSrc, nameDst))
            input()
            clear()
        elif inp == CHANGE_SCORE:
            print("Which of the scores?")
            print(", ".join(typeTable.keys()))
            nameSrc = input()
            if nameSrc in typeTable.keys():
                print("\nTo which score?")
                typeTable[nameSrc][0] = float(input())
                print("Done.")
            else:
                print("Not a valid relationship status")
        elif inp == SHOW_GRAPH:
            if pyvisG is None:
                pyvisG = create_pyvis_graph(df)
            pyvisG.show(FILE_NAME)
            add_scoring(df, FILE_NAME)
            webbrowser.open_new_tab(FILE_NAME)
            print("Done")
        elif inp == EXIT or inp == SAVE:
            df.to_json(DB_NAME)
        else:
            print("Invalid input")
        time.sleep(0.75)


'''
    Creates the leaderboard.
'''
def show_scores(df: pd.DataFrame, output) -> None:
    og_stdout = sys.stdout
    sys.stdout = output  # Change output
    scores = calc_scores(df)
    if 'temp' in scores.keys():
        scores.pop('temp')
    scores = {k: v for k, v in sorted(scores.items(), key=lambda x: (-x[1], -name_counter(df, x[0]), x[0]))}
    i = 0
    lastName = list(scores.keys())[-1]
    print("<b>Leaderboard</b><br>")
    for j, name in enumerate(scores):
        if scores[name] != scores[lastName] or name_counter(df, name) != name_counter(df, lastName):
            i += 1
        lastName = name
        # print(right_parsing(scores, j+1) + str(j+1) + " " + right_parsing(scores, i) + "(" + str(i) + "). " + name + ":", scores[name], "<br>")
        print(right_parsing(scores, i) + str(i) + ". " + name + ":", scores[name], "<br>")
        # print(right_parsing(scores, i) + str(i) + ". " + name + ":", scores[name], "-", name_counter(df, name), "<br>")
    print("Average: %.5f<br>" % statistics.fmean(scores.values()))
    print("Median: " + str(statistics.median(scores.values())) + "<br>")
    print("<br><b>Scoring Table</b><br>")
    for t in [x for x in sorted(typeTable.items(), key=lambda item:item[1][0], reverse=True) if x[0] != "temp"]:
        print(t[0] + ": " + str(t[1][0]) + "<br>")
    sys.stdout = og_stdout  # Return output

'''
    Counts the number of outer neighbors.
'''
def name_counter(df, name) -> int:
    s = df[(name == df['Source']) & ('temp' != df['Target'])].count()[0]
    s += df[(name == df['Target'])].count()[0]
    dfW = df[((df['Source'] == name) | (df['Target'] == name)) & (df['Type'] == 'Wanted')]
    dfWF = dfW.copy().rename(columns={'Source': 'Target', 'Target': 'Source'})
    s -= int((pd.merge(dfW, dfWF, indicator=True, how='outer').query('_merge=="both"').count()[0]) / 2)
    s -= \
    pd.merge(dfW, dfWF, indicator=True, how='outer').query('(_merge=="left_only") & (Target=="' + name + '")').count()[
        0]
    return s

'''
    Returns padding from the right to align the scores.
'''
def right_parsing(scores: dict, i: int) -> str:
    return (int(math.log10(max(len(scores), 1))) + 1 - int(math.log10(max(i, 1)) + 1)) * "&nbsp"

'''
    It adds an edge...
'''
def add_edge(df: pd.DataFrame, src: str, dest: str, t: str) -> pd.DataFrame:
    return pd.concat([df, pd.DataFrame({'Source': [src], 'Target': [dest], 'Type': t})], ignore_index=True, axis=0)

'''
    Adds a node...
'''
def add_node(df: pd.DataFrame, src: str) -> pd.DataFrame:
    return pd.concat([df, pd.DataFrame({'Source': [src], 'Target': ['temp'], 'Type': 'temp'})], ignore_index=True,
                     axis=0)

'''
    Prints the fucking menu.
'''
def print_menu():
    print("1. Add Node")
    print("2. Add Line")
    print("3. Remove Node")
    print("4. Remove Line")
    print("5. Change Node")
    print("6. Change Line")
    print("7. Shortest Path")
    print("8. Change Scoring")
    print("9. Show Graph")
    print("10. Save")
    print("11. Exit")

'''
    Really?
'''
def clear():
    os.system("cls")

'''
    Turns the dataframe to the network.
'''
def create_pyvis_graph(df: pd.DataFrame) -> pyvis.network.Network:
    scores = calc_scores(df)
    maxScore, minScore = max(scores.values()), min(scores.values())
    net = Network(notebook=True, directed=True, height='98%', width='80%')
    for _, row in df.iterrows():
        for name in (row['Source'], row['Target']):
            if name not in net.get_nodes() and name != 'temp':
                net.add_node(name, title=str(scores[name]), size=(abs(scores[name]) + 0.5) * 5, color=toRGB(
                    *choose_color(scores[name], maxScore, minScore, list(sorted(set(scores.values()))))))
        if row['Target'] != 'temp':
            if typeTable[row['Type']][2] == 1:
                net.add_edge(row['Source'], row['Target'], width=0.2 + typeTable[row['Type']][0], title=row['Type'], color=typeTable[row['Type']][1], arrows=None)
            else:
                net.add_edge(row['Source'], row['Target'], width=0.2 + typeTable[row['Type']][0], title=row['Type'], color=typeTable[row['Type']][1])
    # net.show_buttons(filter_=['physics'])
    net.set_options("""
    const options = {
  "physics": {
    "forceAtlas2Based": {
      "springLength": 100
    },
    "minVelocity": 0.75,
    "solver": "forceAtlas2Based"
  }
}""")
    return net

'''
    Creates a graph with networkX. (For calculations)
'''
def create_nx_graph(df: pd.DataFrame) -> nx.Graph:
    elist = list()
    for _, row in df.iterrows():
        elist.append((row['Source'], row['Target'], 1))
        if typeTable[row['Type']][2] == 1 or (row['Target'], row['Source']) in [(s, t) for (s, t, r) in elist]:
            elist.append((row['Target'], "temp: " + row['Source'] + "-" + row['Target'], 1))  # Adding temp node so it won't be a multigraph
            elist.append(("temp: " + row['Source'] + "-" + row['Target'], row['Source'], 0))
    G = nx.DiGraph()
    G.add_weighted_edges_from(elist)
    return G

def shortest_path(G: nx.Graph, nameSrc: str, nameDst: str) -> str:
    s = ""
    try:
        path = min(nx.shortest_simple_paths(G, nameSrc, nameDst, weight="weight"), key=custom_min)
    except nx.exception.NetworkXNoPath:
        return "No path between " + nameSrc + " and " + nameDst
    except nx.exception.NodeNotFound:
        return ""
    for e in path[:-1]:
        if "temp" not in e:
            s += e + " -> "
    s += nameDst
    return s

def custom_min(x):
    return len([t for t in x if 'temp' not in t])

'''
    Imbeds the leader into the file.
'''
def add_scoring(df: pd.DataFrame, file_name: str) -> None:
    with open(file_name, "r") as f:  # Delete the last two lines of the file
        f.seek(0, os.SEEK_END)
        pos = f.tell() - 1
        count = 3
        counter = 0
        while pos > 0 and count > 0:
            if f.read(1) == "\n":
                count -= 1
            pos -= 1
            f.seek(pos, os.SEEK_SET)
    with open(file_name, "a") as f:
        # f.write("\n<div id=\"\" style=\"overflow:auto; height:98%;\">\n<tt>\n<font size=\"+2\">")
        f.seek(pos, os.SEEK_SET)
        f.truncate()
        f.write("\n<div id=\"\" style=\"overflow:auto; height:98%;\">\n<tt>")
        show_scores(df, f)
        f.write("</font>\n</tt>\n</div>\n</body>\n</html>")

'''
    Formats RGB values to work with the code.
'''
def toRGB(r: int, g: int, b: int) -> str:
    return "#{:02x}{:02x}{:02x}".format(r, g, b)

'''
    Calculates the fitting color.
'''
def choose_color(s: float, M: int, m: int, scale: list) -> (int, int, int):
    neg, pos = [], []
    for x in scale:
        (neg, pos)[x >= 0].append(x)
    if s in neg:
        return int(255 * (len(neg) - neg.index(s)) / len(neg)), 0, 0
    else:
        return 0, int(255 * pos.index(s) / len(pos)), int(255 * pos.index(s) / len(pos))

'''
    Creates the leaderboard.
'''
def calc_scores(df: pd.DataFrame) -> dict:
    scores = {}
    for _, row in df.iterrows():
        if row['Source'] not in scores.keys():
            scores[row['Source']] = 0.0
        if row['Target'] not in scores.keys():
            scores[row['Target']] = 0.0
        if typeTable[row['Type']][2] == 0:
            if ((df['Source'] == row['Target']) & (df['Target'] == row['Source'])).any():  # Point at each other
                scores[row['Source']] += typeTable[row['Type']][0]
            else:
                scores[row['Source']] -= typeTable[row['Type']][0]
                scores[row['Target']] += typeTable[row['Type']][0]
        else:
            scores[row['Source']] += typeTable[row['Type']][0]
            scores[row['Target']] += typeTable[row['Type']][0]
    for name in scores.keys():
        scores[name] = round(scores[name], 2)
    return scores

'''
    Changes a nodes name.
'''
def change_name(df: pd.DataFrame, nameSrc: str, nameDst: str) -> None:
    df.loc[df["Source"] == nameSrc, "Source"] = nameDst
    df.loc[df["Target"] == nameSrc, "Target"] = nameDst

'''
    Changes a lines title.
'''
def change_line(df: pd.DataFrame, nameSrc: str, nameDst: str, rel: str) -> None:
    df.loc[((df["Source"] == nameSrc) & (df["Target"] == nameDst)), "Type"] = rel
    if typeTable[rel][2] == 1:
        df.loc[((df["Target"] == nameSrc) & (df["Source"] == nameDst)), "Type"] = rel




main()
